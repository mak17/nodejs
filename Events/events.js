const eventEmitter = require("events");

// create emmiter
var emitter = new eventEmitter();

// on event 'message' trigger the function
// first parameter is the name of event
// second parameter is the function which will be trigger on event
emitter.on("message", (msg) => console.log(msg))

// by use this we trigger our event
// first parameter is the name of the event which we want run
// second paramtera is a argument for the function from event
emitter.emit("message", "test test");