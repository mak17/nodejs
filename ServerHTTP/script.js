const http = require("http");

const server = http.createServer(function(request, response){
	//response.write("Hello World!");
	//response.end();
	
	var body = null;
	var status = 200;
	
	if(request.url == "/") {
		body = "Strona g�owna";
	} else if (request.url == "/test") {
		body = "Test";
	} else {
		status = 404;
		body = "Nie znaleziono";
	}
	
	response.writeHead(status, {
		//main type
		"Content-Type": "text/html"
	});
	
	response.write(body);
	//response.write("<h1>Test</h1>");
	response.end();
});

server.listen(8000, function(){
	console.log("Serwer zosta� uruchominy pod adresem: http//localhost:8000");
});