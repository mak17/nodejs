const fs = require("fs");

var stream = fs.createReadStream("./test.txt");

// chunk is buffer
stream.on("data", chunk => {
	console.log(chunk.toString());
})

// end - triggers on the end of file
stream.on("end", () => console.log("End file"));