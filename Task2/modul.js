//creating a module
var message = "Modu� add.js";

console.log(message);

//create the method
function add(n1, n2) {
	return n1 + n2;
}

// export method. It is important when we want use out method in 
// others files. If the names are te same we can use onlu one name
// but if want use other name we shoud use:
// module.exports = {
//	newNameWhichWeBeAvailableOutsideThisFile : OurMethod_(add)
// };
module.exports = {
	add
};